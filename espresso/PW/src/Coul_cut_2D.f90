!
! Copyright (C) 2007-2011 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
! 
! TSoh new module for all the variables and subroutines necessary to 2D code
!----------------------------------------------------------------------------
MODULE Coul_cut_2D
  !----------------------------------------------------------------------------
  !
  ! ... this module contains the variables and subroutines needed for the 
  ! ... 2D Coulomb cutoff technique 
  !
  USE kinds, ONLY :  DP
  USE constants, ONLY : tpi, pi
  SAVE
  !
  LOGICAL :: do_cutoff_2D=.FALSE.
  real(DP) :: lz
  ! TS the cutoff factor (1- e^{- G_p (c/2))} cos (Gzlz) )
  ! needed to cutoff Coulomb interaction in 2D materials 
  !
  REAL(DP), ALLOCATABLE :: cutoff_2D(:)
  REAL(DP), ALLOCATABLE :: lr_Vloc(:,:)
  
  ! 
  !
 
  
  
CONTAINS

!
! TS new subroutine to define cutoff factor in 2D simulations
!----------------------------------------------------------------------
subroutine cutoff_fact ()
  !----------------------------------------------------------------------
  !
  !   calculate the cutoff factor of the Coulomb interaction for 2D
  !   calculations   
  !
  USE kinds
  USE io_global, ONLY : stdout
  USE gvect,     ONLY : g, ngm, ngmx
  USE cell_base, ONLY : alat, celldm, at
  implicit none
  !
  !    here the local variables
  integer :: ng, i
  ! counter over G vectors
  real(DP) :: Gzlz, Gplz
  ! WRITE 2D code message
  WRITE(stdout, *) "----2D----2D----2D----2D----2D----2D"
  WRITE(stdout, *) "THE CODE IS RUNNING WITH 2D CUTOFF"
  WRITE(stdout, *) "----2D----2D----2D----2D----2D----2D"
  ALLOCATE( cutoff_2D (ngmx) ) 
  ! This does not work for every bravais lattice. 
  ! Only the ones such that
  ! v3=(0,0,celldm(3)/a)
  !lz=celldm(3)/2.0d0*alat
  !
  !  at(:,i) are the lattice vectors of the simulation cell, a_i,
  !  in alat units: a_i(:) = at(:,i)/alat
  do i=1,2
     IF (abs(at(3,i))>1d-8) WRITE(stdout, *) "2D CODE WILL NOT WORK, 2D MATERIAL NOT IN X-Y PLANE!!"
  enddo
  lz=0.5d0*at(3,3)*alat
  do ng = 1, ngm
     Gplz=SQRT( g (1, ng)**2 + g (2, ng)**2 )*tpi*lz/alat
     Gzlz= g (3, ng)*tpi*lz/alat
     cutoff_2D(ng)= 1.0d0- exp(-Gplz)*cos(Gzlz)
  enddo
  !
  return
end subroutine cutoff_fact
!
! TS new subroutine to define cutoff long range part of vloc
!----------------------------------------------------------------------
subroutine cutoff_lr_Vloc ( )
  !----------------------------------------------------------------------
  !
  !   calculate the long-range part of vloc(g) cutoff for 2D calculations
  !
  USE kinds
  USE constants,   ONLY : fpi, e2, eps8
  USE fft_base,    ONLY : dfftp
  USE gvect,      ONLY : ngm, gg, g, ngmx
  ! TSoh  gg=G^2 in increasing order (in units of tpiba2=(2pi/a)^2)
  USE ions_base,  ONLY : zv, nsp
  USE uspp_param, ONLY : upf
  USE cell_base,  ONLY : omega, tpiba2
  USE io_global, ONLY: stdout
  implicit none
  !
  ! local variable
  integer :: ng, nt, ng0 !, ngstart
  REAL(DP)::fac
  ! counter over G vectors
  !      WRITE(stdout, *) "thibault : Calculating cutoff lr vloc  ngmx=", ngm
  IF (.not.ALLOCATED(lr_Vloc)) ALLOCATE( lr_Vloc (ngmx,nsp) )
  lr_Vloc(:,:)=0.0d0
  IF (gg(1)<eps8) THEN
     lr_Vloc (1, :)=0.0d0
     ng0=2
  ELSE
     ng0=1
  ENDIF
  DO nt = 1, nsp
     fac= upf(nt)%zp * e2 / tpiba2
     DO ng = ng0, ngm
        lr_Vloc (ng, nt)= - fpi / omega* fac * cutoff_2D(ng)* &
                      & exp ( - gg (ng) * tpiba2 * 0.25d0) / gg (ng)
     END DO
  END DO
  !
  return
end subroutine cutoff_lr_Vloc
!
! TS new subroutine to re-add cutoff long-range vloc
!----------------------------------------------------------------------
subroutine cutoff_local ( aux )
  !----------------------------------------------------------------------
  !
  !   re-add cutoff long-range part of vloc   
  !
  USE kinds
  USE fft_base,    ONLY : dfftp
  USE gvect,      ONLY : ngm, nl
  USE vlocal,    ONLY : strf
  USE ions_base,  ONLY : nsp
  USE io_global, ONLY: stdout
  implicit none
  !
  COMPLEX(DP), INTENT(INOUT):: aux (dfftp%nnr)
  ! input : local potential
  ! local variable
  integer :: ng, nt !, ngstart
  ! counter over G vectors
  !      WRITE(stdout, *) "thibault : Calculating cutoff local, ngm=", ngm
  !
  DO nt = 1, nsp
     DO ng = 1, ngm
        aux (nl(ng))=aux(nl(ng)) + lr_Vloc(ng, nt) * strf (ng, nt)
     END DO
  END DO
  !
  return
end subroutine cutoff_local
!
! TS new subroutine to define cutoff of Hartree potential and hrtree energy
! in G-space
!----------------------------------------------------------------------
subroutine cutoff_hartree (rhog, aux1, ehart)
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE gvect,      ONLY : ngm, gg , gstart
  USE lsda_mod,  ONLY : nspin
  USE io_global, ONLY: stdout
  implicit none
  ! 
  REAL(DP), INTENT(INOUT):: aux1( 2, ngm )
  COMPLEX(DP), INTENT(IN):: rhog(ngm,nspin)
  REAL(DP),    INTENT(INOUT) :: ehart
  ! input : local potential
  ! local variable
  integer :: ig
  ! counter over G vectors
  REAL(DP)::fac
  REAL(DP):: rgtot_re, rgtot_im
  !WRITE(stdout, *) "thibault : Calculating cutoff hartree"
  
  DO ig = gstart, ngm
     !
     fac = 1.D0 / gg(ig)* cutoff_2D(ig)
     !
     rgtot_re = REAL(  rhog(ig,1) )
     rgtot_im = AIMAG( rhog(ig,1) )
     !
     IF ( nspin == 2 ) THEN
        !
        rgtot_re = rgtot_re + REAL(  rhog(ig,2) )
        rgtot_im = rgtot_im + AIMAG( rhog(ig,2) )
        !
     END IF
     !
     ehart = ehart + ( rgtot_re**2 + rgtot_im**2 ) * fac
     !
     aux1(1,ig) = rgtot_re * fac
     aux1(2,ig) = rgtot_im * fac
     !
  ENDDO
!  ehart = ehart -  ( nelec- SUM( zv(ityp(1:nat)) )  )**2 /  &
!           ABS((at(1,1)*at(2,2)-at(2,1)*at(1,2))*alat**2)*tpiba* lz*0.5
  return
end subroutine cutoff_hartree
!
! TS new subroutine to define cutoff of ewald sum in G space
!----------------------------------------------------------------------
subroutine cutoff_ewald (charge, alpha, ewaldg, omega)
  !----------------------------------------------------------------------
  !
  !   calculate the cutoff factor of the Coulomb interaction for 2D
  !   calculations   
  !
  USE kinds
  USE gvect,      ONLY : ngm, gg , gstart
  USE ions_base,  ONLY : zv, nsp, nat, ityp
  USE cell_base,  ONLY : tpiba2, alat
  USE vlocal,    ONLY : strf
  USE io_global, ONLY: stdout
  implicit none
  !
  REAL(DP),    INTENT(INOUT) :: ewaldg
  REAL(DP),    INTENT(IN) :: charge
  REAL(DP),    INTENT(IN) :: alpha
  REAL(DP),    INTENT(IN) :: omega
  ! input : local potential
  ! local variable
  integer :: ng, nt, na, nr, ir , iz, nz, rmax
  ! counter over G vectors
  COMPLEX(DP):: rhon
  REAL(DP)  ::  rp, z
  real(DP), external :: qe_erf
  ! G=0 componenent of the long-range part of the local part of the 
  ! pseudopot
  ! minus the Hartree potential is set to 0 !!! THIS IS IMPORTANT
  ewaldg = 0.0d0
  do ng = gstart, ngm
     rhon = (0.d0, 0.d0)
     do nt = 1, nsp
        rhon = rhon + zv (nt) * CONJG(strf (ng, nt) )
     enddo
     ewaldg = ewaldg +  abs (rhon) **2 * exp ( - gg (ng) * tpiba2 /&
          alpha / 4.d0) / gg (ng)*cutoff_2D(ng) / tpiba2
  enddo
  ewaldg = 2.d0 * tpi / omega * ewaldg
  !
  !  Here add the other constant term (Phi_self)
  !
  if (gstart.eq.2) then
     do na = 1, nat
        ewaldg = ewaldg - zv (ityp (na) ) **2 * sqrt (8.d0 / tpi * &
             alpha)
     enddo
  endif
  !  
  return
end subroutine cutoff_ewald
!
! TS new subroutine cutoff the ewald part of forces
!----------------------------------------------------------------------
subroutine cutoff_force_ew (aux, alpha)
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE gvect,      ONLY : ngm, gg , gstart
  USE cell_base,  ONLY : tpiba2, alat
  USE io_global, ONLY: stdout
  implicit none
  !
  COMPLEX(DP),    INTENT(INOUT) :: aux(ngm)
  REAL(DP),    INTENT(IN) :: alpha
  integer :: ig
  ! counter over G vectors
  !  WRITE(stdout,*) "thibault test: cutting off Ewald FORCES"
  do ig = gstart, ngm
     aux (ig) = aux (ig) * exp ( - gg (ig) * tpiba2 / alpha / 4.d0) &
                    / (gg (ig) * tpiba2) * cutoff_2D(ig)
  enddo	  
  return
end subroutine cutoff_force_ew
!
! TS new subroutine cutoff the local part of the forces
!----------------------------------------------------------------------
subroutine cutoff_force_lc ( aux, forcelc )
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE gvect,      ONLY : ngm, gg, g , gstart, nl
  USE constants,   ONLY : fpi, e2, eps8, tpi
  USE uspp_param, ONLY : upf 
  USE cell_base,  ONLY : tpiba2, alat, omega
  USE ions_base,     ONLY : nat, zv, tau, ityp
  USE io_global, ONLY: stdout
  USE fft_base,    ONLY : dfftp
  implicit none
  !
  COMPLEX(DP),    INTENT(IN) :: aux(dfftp%nnr)
  REAL(DP),    INTENT(INOUT) :: forcelc (3, nat)
  REAL(DP)::  fact, arg
  integer :: ig, na, ipol
  !  WRITE(stdout,*) "thibault test: cutting off LOCAL POT FORCES"
  fact = 1.d0
  do na = 1, nat
     do ig = gstart, ngm 
        arg = (g (1, ig) * tau (1, na) + g (2, ig) * tau (2, na) + &
               g (3, ig) * tau (3, na) ) * tpi
        do ipol = 1, 3
           forcelc (ipol, na) = forcelc (ipol, na) + fact * tpi / alat * &
                 g (ipol, ig) * lr_Vloc(ig, ityp(na)) * omega* &
                (sin(arg)*DBLE(aux(nl(ig))) + cos(arg)*AIMAG(aux(nl(ig))) )
        enddo
     enddo 
  enddo
  !
  return
end subroutine cutoff_force_lc
!
! TS new subroutine cutoff loc stress
!----------------------------------------------------------------------
subroutine cutoff_stres_evloc ( psic_G, evloc )
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE ions_base,  ONLY : ntyp => nsp
  USE vlocal,     ONLY : strf
  USE gvect,      ONLY : ngm , gstart, nl
  USE io_global,  ONLY : stdout
  USE fft_base,   ONLY : dfftp
  implicit none
  !
  COMPLEX(DP),    INTENT(IN) :: psic_G(dfftp%nnr)
  REAL(DP),    INTENT(INOUT) :: evloc
  integer :: ng, nt
  !   WRITE(stdout,*) "thibault test: cutting off LOCAL POT STRESS"
  ! If gstart=2, it means g(1) is G=0, but we have nothing to add for G=0
  ! So we start at gstart.
  do nt = 1, ntyp
     do ng = gstart, ngm
        evloc = evloc +  DBLE (CONJG(psic_G (nl (ng) ) ) * strf (ng, nt) ) &
           * lr_Vloc (ng, nt) 
     enddo
  enddo
  !
  return
end subroutine cutoff_stres_evloc
!
! TS new subroutine cutoff the local part of the stress
!----------------------------------------------------------------------
subroutine cutoff_stres_sigmaloc (psic_G, sigmaloc )
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE ions_base,  ONLY : ntyp => nsp
  USE vlocal,     ONLY : strf
  USE constants,   ONLY :  eps8
  USE gvect,      ONLY : ngm ,g, gg, gstart, nl
  USE cell_base,  ONLY :tpiba, tpiba2, alat, omega
  USE io_global,  ONLY : stdout
  USE fft_base,   ONLY : dfftp
  implicit none
  !
  COMPLEX(DP),    INTENT(IN) :: psic_G(dfftp%nnr)
  REAL(DP),    INTENT(INOUT) :: sigmaloc(3,3)
  integer :: ng, nt, l, m
  REAL(DP)::Gp, G2lzo2Gp, add_term, dlr_Vloc
  !  WRITE(stdout,*) "thibault test: cutting off LOCAL POT STRESS sigmaloc"
  ! no G=0 contribution
  ! g(1) shouldn't be a problem even if it's G=0
  ! fact=1.0d0
  do nt = 1, ntyp
     do ng = gstart, ngm
        Gp=SQRT( g (1, ng)**2 + g (2, ng)**2 )*tpiba
        IF(Gp<eps8) then
           G2lzo2Gp=0.0d0
           add_term=0.0d0
        ELSE
           G2lzo2Gp=gg(ng)*tpiba2*lz/2.0d0/Gp
           add_term=-G2lzo2Gp*(1.0d0-cutoff_2D(ng))/cutoff_2D(ng)
        ENDIF
        ! G^2*lz/2|Gp|
        ! cutoff_2D(ng)=0 when Gp=0 and Gzlz=Npi. 
        ! Thus the expression below doesn't work but 
        ! add_term is supposed to be zero when Gp=0 anyway.
        !|G^2*lz/2Gp|* e^(-Gplz)cos(Gzlz)/(1-e^(-Gplz)cos(Gzlz)) 
        ! this term comes from the derivative of the cutoff factor wrt Gp^2
        !dlr_Vloc= d(lr_Vloc)/ d(G^2) see dvloc_of_g and Eq2 PRB 32 3792
        ! re-check signs
        do l = 1, 3
           if (l.eq.3) then
              dlr_Vloc= - 1.0d0/ (gg(ng)*tpiba2) * lr_Vloc(ng,nt)  &
                               * (1.0d0+ gg(ng)*tpiba2/4.0d0)
           else
              dlr_Vloc= - 1.0d0/ (gg(ng)*tpiba2) * lr_Vloc(ng,nt)  &
                               * (1.0d0+ add_term + gg(ng)*tpiba2/4.0d0)
           endif
           do m = 1, l
              sigmaloc(l, m) = sigmaloc(l, m) +  DBLE( CONJG( psic_G(nl(ng) ) ) &
                   * strf (ng, nt) ) * 2.0d0 * dlr_Vloc  &
                       * tpiba2 * g (l, ng) * g (m, ng) 
           enddo
        enddo
     enddo
  enddo
  !
  return
end subroutine cutoff_stres_sigmaloc
! TS new subroutine cutoff the hatree part of the stress
!----------------------------------------------------------------------
subroutine cutoff_stres_sigmahar (psic_G, sigmahar )
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE gvect,      ONLY : ngm ,g, gg, gstart, nl
  USE constants,   ONLY :  eps8
  USE cell_base,  ONLY : tpiba2, alat, tpiba
  USE io_global,  ONLY : stdout
  USE fft_base,   ONLY : dfftp
  implicit none
  !
  COMPLEX(DP),    INTENT(IN) :: psic_G(dfftp%nnr)
  REAL(DP),    INTENT(INOUT) :: sigmahar(3,3)
  integer :: ng, nt, l, m
  REAL(DP)::Gp, G2lzo2Gp, add_term, shart, g2, fact
  !   WRITE(stdout,*) "thibault test: cutting off LOCAL POT STRESS sigmhar"
  ! g(1) is a problem if it's G=0, because we divide by G^2. So start at gstart
  do ng = gstart, ngm
     Gp=SQRT( g (1, ng)**2 + g (2, ng)**2 )*tpiba
     IF(Gp<eps8) then
        G2lzo2Gp=0.0d0
        add_term=0.0d0
     ELSE
        G2lzo2Gp=gg(ng)*tpiba2*lz/2.0d0/Gp
        add_term=-G2lzo2Gp*(1.0d0-cutoff_2D(ng))/cutoff_2D(ng)
     ENDIF
     !|G^2*lz/2Gp|* e^(-Gplz)cos(Gzlz)/(1-e^(-Gplz)cos(Gzlz))
     ! G^2*Derivative of the cutoff factor wrt Gp^2 basically 
     g2 = gg (ng) * tpiba2
     shart = psic_G (nl (ng) ) * CONJG(psic_G (nl (ng) ) ) / g2 * cutoff_2D(ng)
     do l = 1, 3
        if (l.eq.3) then
           fact=1.0d0
        else
           fact=1.0d0+add_term
        endif
        do m = 1, l
           sigmahar (l, m) = sigmahar (l, m) + shart * tpiba2 * 2 * &
                   g (l, ng) * g (m, ng) / g2  * fact
        enddo
     enddo
  enddo
  !sigma is multiplied by 0.5*fpi*e2 after
  return
end subroutine cutoff_stres_sigmahar
! TS new subroutine cutoff the ewald part of the stress
!----------------------------------------------------------------------
subroutine cutoff_stres_sigmaewa (alpha, sdewald, sigmaewa )
  !----------------------------------------------------------------------
  !
  !
  USE kinds
  USE ions_base,  ONLY : nat, zv, tau, ityp
  USE constants,   ONLY : e2, eps8
  USE gvect,      ONLY : ngm ,g, gg, gstart, nl
  USE cell_base,  ONLY : tpiba2, alat, omega, tpiba
  USE io_global,  ONLY : stdout
  implicit none
  !
  REAL(DP),       INTENT(IN) :: alpha
  REAL(DP),       INTENT(INOUT) :: sigmaewa(3,3)
  REAL(DP),       INTENT(INOUT) :: sdewald
  integer :: ng, na, l, m
  REAL(DP)::Gp, G2lzo2Gp, add_term, sewald, g2, g2a, arg, fact
  complex(DP) :: rhostar
  !
  !   WRITE(stdout,*) "thibault test: cutting off LOCAL POT STRESS sigmaewa"
  ! g(1) is a problem if it's G=0, because we divide by G^2. So start at gstart
  ! fact=1.0d0, gamma_only not implemented
  ! G=0 componenent of the long-range part of the local part of the 
  ! pseudopotminus the Hartree potential is set to 0 !!! 
  ! in other words, sdewald=0.  THIS IS IMPORTANT!
  ! sdewald is the last term in equation B1 of PRB 32 3792
  sdewald=0.0D0
  do ng = gstart, ngm
     Gp=SQRT( g (1, ng)**2 + g (2, ng)**2 )*tpiba
     IF(Gp<eps8) then
        G2lzo2Gp=0.0d0
        add_term=0.0d0
     ELSE
        G2lzo2Gp=gg(ng)*tpiba2*lz/2.0d0/Gp
        add_term=-G2lzo2Gp*(1.0d0-cutoff_2D(ng))/cutoff_2D(ng)
     ENDIF
     g2 = gg (ng) * tpiba2
     g2a = g2 / 4.d0 / alpha
     rhostar = (0.d0, 0.d0)
     do na = 1, nat
        arg = (g (1, ng) * tau (1, na) + g (2, ng) * tau (2, na) + &
            g (3, ng) * tau (3, na) ) * tpi
        rhostar = rhostar + zv (ityp (na) ) * CMPLX(cos (arg), sin (arg),kind=DP)
     enddo
     rhostar = rhostar / omega
     sewald = tpi * e2 * exp ( - g2a) / g2* cutoff_2D(ng) * abs (rhostar) **2
     ! Sewald is an other diagonal term that is similar to the diagonal term 
     ! the other stress contributions. It basically gives a term prop to 
     ! the ewald energy
     sdewald = sdewald-sewald
     do l = 1, 3
        if (l.eq.3) then
           fact=(g2a + 1.0d0)
        else
           fact=(1.0d0+g2a+add_term)
        endif
        !
        do m = 1, l
           sigmaewa (l, m) = sigmaewa (l, m) + sewald  * tpiba2 * 2.d0 * &
                 g (l, ng) * g (m, ng) / g2 * fact
        enddo
     enddo
  enddo
  return
end subroutine cutoff_stres_sigmaewa
END MODULE Coul_cut_2D
