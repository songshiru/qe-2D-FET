!
! Copyright (C) 2003-2010 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
! Gate potentials, forces and energy for FET setup in 2D code
! Thibault Sohier
!
!-------------------------------------------------------------
SUBROUTINE add_gates(Vpot)
  !-----------------------------------------------------------
  !
  !
  !
  USE kinds,         ONLY : DP
  USE extfield,      ONLY : FETsetup, barriers, zbot, bot_charge, ztop, top_charge, &
                            barrier_height, zbot_barrier, ztop_barrier, &
                            forcegate, relaxz, eopreg
  USE ener,          ONLY : egate
  USE cell_base,     ONLY : alat, at , omega, bg , trianglepot
  USE klist,         ONLY : nelec  
  USE constants,     ONLY : fpi, eps8, e2, tpi, pi
  USE ions_base,     ONLY : nat, ityp, zv, tau
  USE force_mod,     ONLY : lforce
  USE io_global,     ONLY : stdout,ionode
  !USE control_flags, ONLY : mixing_beta
  USE lsda_mod,      ONLY : nspin
  USE mp_global,     ONLY : me_bgrp
  USE fft_base,      ONLY : dfftp
  USE Coul_cut_2D,   ONLY : do_cutoff_2D
  
  IMPLICIT NONE
  !
  ! io variables
  !
  REAL(DP), INTENT(INOUT):: Vpot(dfftp%nnr) ! total local external potential 
                                            ! (Vltot in input, Vltot+gates in output)
  !
  ! local variables
  !
  REAL(DP) :: c, lz, e_gi, e_gg, Vbot, Vtop, Vbar, ionic_charge, mat_charge, edge
  ! c : distance between periodic images
  ! lz : cutoff distance (half the distance between periodic images)
  ! e_gi : energy of the ions in the field of the gates
  ! e_gg : energy of the gates in their own potentials
  ! Vbot, Vtop : potentials of top and bottom gates
  ! Vbar: barrier potential
  ! ionic_charge : charge of the ions plane
  ! mat_charge: charge of the material (=tot_charge)
  ! edge: edge of the barrier, distance over which the barrier's edge is linear 
  REAL(DP) :: area, z, bot_amp, top_amp, zb, zat
  INTEGER :: idx,idx0, i, k, na, ir
  !------------------------
  !Variables check
  !------------------------
  ! 
  IF (.NOT.FETsetup) RETURN
  IF (.NOT. do_cutoff_2D) CALL errore( 'add_gates', '2D flag do_cutoff_2D needed',1 ) 
  !
  !-------------------------
  ! Variable initialization
  !------------------------
  !
  c=at(3,3)*alat
  !omega= cell volume (in au^3) => area= cell area in au^2
  area=omega/c
  !
  !initialize charges
  ionic_charge= SUM( zv(ityp(1:nat)) )
  ! mat_charge is equal to tot_charge in a PW run.
  ! tot_charge is well defined during a PW run (and could be used from klist)
  ! because it was entered as input,
  ! BUT it is not defined for pp of ph run, as it it not passed as xml data. 
  ! Here we define mat_charge using ionic charge and nelec 
  ! because those are the arguments passed as xml data.
  mat_charge=ionic_charge-nelec
  !
  ! charge of top gate must be so that the system is neutral overall 
  top_charge=-(bot_charge+mat_charge)
  bot_amp=tpi*e2*bot_charge/area
  top_amp=tpi*e2*top_charge/area
  !
  ! redefine cutoff distance
  lz = 0.5d0*c
  !
  ! use eopreg if defined corretly in input
  IF (abs(eopreg)>eps8) THEN
    edge=eopreg
  ELSE ! otherwise use reasonnable default value
    edge=0.1d0
  ENDIF
  !
  ! Index for parallel summation
  idx0 = 0
#if defined (__MPI)
  DO i = 1, me_bgrp
     idx0 = idx0 + dfftp%nr1x*dfftp%nr2x*dfftp%npp(i)
  END DO
#endif
  !
  !-----------------------
  ! Generate potential
  !-----------------------
  !
  ! Add top and bottom gate potentials to the input potential
  DO ir = 1, dfftp%nnr
    Vbot=0.0d0
    Vtop=0.0d0
    Vbar=0.0d0
    !
    ! ... three dimensional indexes
    !
    idx = idx0 + ir - 1
    k   = idx / (dfftp%nr1x*dfftp%nr2x)
    z = DBLE(k)/DBLE(dfftp%nr3)
    ! z is units of c, the distance between perio images
    !
    !bottom gate potential
    !
    Vbot=bot_amp*trianglepot(z,zbot)*c
    !
    !top gate potential
    !
    Vtop=top_amp*trianglepot(z,ztop)*c
    !
    IF (barriers) THEN
      ! barrier potential
      zb=z
      IF (z>0.5d0)  zb=z-1.0d0
      !IF (z<-0.5d0) zb=z+1.0d0
      ! z=-0.5....zbot_barrier........ztop_barrier......+0.5
      !                 ||                ||             ||
      !      barrier    ||     system     ||    barrier  ||
      IF (zb<=zbot_barrier) THEN
        IF (zb>=(zbot_barrier-edge)) THEN
          Vbar=(zbot_barrier-zb)/(edge) * barrier_height
        ELSE
          Vbar= barrier_height
        ENDIF
      ENDIF
      IF (zb>=ztop_barrier) THEN
        IF (zb<=(ztop_barrier+edge)) THEN
          Vbar=(zb-ztop_barrier)/(edge) * barrier_height
        ELSE
          Vbar= barrier_height
        ENDIF
      ENDIF
      !
    ENDIF
    !
    !Adding up everything. (Vpot already containing the ionic potential Vltot)
    Vpot(ir) = Vpot(ir) + Vbot + Vtop + Vbar
  END DO
  ! 
  !-----------------------
  ! Compute energies
  !----------------------
  !
  ! gate on ions
  e_gi=0.d0
  DO na = 1, nat
    zat= tau(1,na)*bg(1,3)+tau(2,na)*bg(2,3)+tau(3,na)*bg(3,3)
    e_gi = e_gi - zv(ityp(na)) * c * & 
                     &(bot_amp*trianglepot(zat,zbot)+top_amp*trianglepot(zat,ztop))
  ENDDO
  !
  ! gate on gate
  e_gg= (top_charge**2+bot_charge**2)/area*pi*e2*lz/2 + & 
                  tpi*e2*top_charge*bot_charge/area*(-abs(ztop-zbot)*c+lz/2)
  !
  !total
  egate = e_gi + e_gg
  !
  !-------------------------
  ! compute forces 
  !-------------------------
  IF (lforce) THEN
     forcegate=0.0d0
     DO na=1,nat
        zat= tau(1,na)*bg(1,3)+tau(2,na)*bg(2,3)+tau(3,na)*bg(3,3)
        forcegate(3,na)=zv(ityp(na))*(bot_amp*(zat-zbot)/abs(zat-zbot) + &
                                        top_amp*(zat-ztop)/abs(zat-ztop))
     ENDDO
  ENDIF
  !
  !--------------------------
  ! WRITTING INFO ON output
  !--------------------------
  !
  IF (ionode) THEN
    !
    ! Output data
    !
    WRITE( stdout,*)
    WRITE( stdout,'(5x,"Adding gates in the form of charged planes to simulate FET")')
    WRITE( stdout,'(5x,"works only for 2D systems periodic in the plane")')
    WRITE( stdout, '(8x,"prefactor of the top gate potentials [Ry]: ", f12.6)') top_amp 
    WRITE( stdout, '(8x,"prefactor of the bottom gate potentials [Ry]: ", f12.6)') bot_amp
    WRITE( stdout, '(8x,"   position of the top gates within cell (c):    ", f8.5)') ztop
    WRITE( stdout, '(8x,"   position of the bottom  gates within cell (c):    ", f8.5)') zbot
    WRITE( stdout, '(8x," gate-ions + gate-gate  contributions to energy", f12.6)') egate
    !
    IF ((lforce).AND.(relaxz)) THEN
       WRITE( stdout,*)
       WRITE( stdout,'(8x,"Allow relaxation in z-direction (i.e. disabled control for total force = 0) ")')
    ENDIF
    !
    IF (barriers) THEN
       WRITE( stdout,*)
       WRITE( stdout,'(8x,"Adding barriers to prevent charge spilling into region of the monopole")')
       WRITE( stdout,'(8x,"barrier_height = ", f8.5," in Ry")') barrier_height
       WRITE( stdout,*) "bot barrier, top barrier", zbot_barrier, ztop_barrier
       WRITE( stdout,*) "edge=", edge
    ENDIF
     
    WRITE( stdout,*)     
  ENDIF
  RETURN
  
  END SUBROUTINE add_gates 
